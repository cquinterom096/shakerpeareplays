defmodule ShakespearePlays do
  require IEx
  Envy.load(['secret.env'])

  def scraper do
    HTTPoison.start # Need to start the service (investigate why wtf)
    download() # Obtains all the xml doc
    |> parse_acts
    |> count_lines_per_speech
    |> compact_line_count
  end

  def download do
    HTTPoison.get!(System.get_env("PLAY"))
  end

  defp parse_acts(%{body: body, headers: _, request_url: _,status_code: _}) do
    body
    |> Floki.find("act")
    |> Floki.find("speech")
  end

  defp count_lines_per_speech(speeches, spoken_ocurrence \\ %{}) do
    for speech <- speeches do
      lines = current_lines(speech)
      Map.update(
        spoken_ocurrence,
        current_speaker(speech),
        lines, &(&1 + lines)
      )
    end
  end

  defp current_speaker(speech) do
    speech |> elem(2) |> List.first |> elem(2) |> List.first
  end

  defp current_lines(speech) do
    speech |> elem(2) |> length |> number_of_lines
  end

  defp number_of_lines(current_length) do
    current_length - 1
  end

  defp compact_line_count(lines_per_speaker, compacter \\ []) do
    Enum.map(lines_per_speaker, fn(map) -> compacter ++ Map.keys(map) end)
    |> List.flatten
    |> Enum.uniq
    |> add_lines_per_speaker(lines_per_speaker)
    |> print_speakers
  end

  # Array of speakers and arrays of maps of speakers
  defp add_lines_per_speaker(speakers, all_speakers_lines) do
    Enum.map(speakers, fn(speaker) -> add_speaker_lines(speaker, all_speakers_lines) end)
  end

  defp add_speaker_lines(speaker, all_speakers_lines) do
    total_lines = Enum.reduce(all_speakers_lines, 0, fn(map, acum) -> if speaker == extract_key(map), do: map[speaker] + acum, else: 0 + acum end)
    %{"#{speaker}": total_lines}
  end

  defp print_speakers(total_lines_per_speaker) do
    Enum.each(total_lines_per_speaker, fn(map) -> IO.puts "The #{extract_key(map)} spoke a total of #{map[extract_key(map)]}" end)
  end

  defp extract_key(map) do
    map
    |> Map.keys
    |> List.first
  end
end
