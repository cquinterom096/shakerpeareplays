defmodule Mix.Tasks.InitPlayScraper do
  use Mix.Task

  @shortdoc "Prints the number of lines spoken by each character in the play."
  def run(_) do
    ShakespearePlays.scraper
  end
end
