defmodule ShakespearePlays.MixProject do
  use Mix.Project


  def project do
    [
      app: :shakespeare_plays,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(), default_task: "init_play_scraper"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0"},
      {:envy, "~> 1.1.1"},
      {:sweet_xml, "~> 0.6.5"},
      {:elixir_xml_to_map, "~> 0.1"},
      {:exoml, "~> 0.0.2"},
      {:floki, "~> 0.20.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
